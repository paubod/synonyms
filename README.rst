Synonyms
========

This software produces data containing gene synonyms from Entrez Gene's
``gene_info`` file that can be incorporated into the JASPAR and UniBind
databases.

Details of the workflow are provided in a separate `workflow`_ document. See
the `deployment`_ documentation for a convenient way of incorporating this
workflow into the deployment of other software.

.. _`deployment`: docs/Deployment.rst
.. _`workflow`: docs/Workflow.rst


Preparing Synonyms Data
-----------------------

A number of tools are involved in preparing synonyms data for other programs.
First of all, a tool is used to download required data as follows:

.. code:: shell

  tools/data/download_data

This should put the required data in the current directory, although an
alternative location can be specified.

Then, a tool is used to prepare the data inputs to synonym generation:

.. code:: shell

  tools/data/prepare_data

Here, it can be useful to specify the location of the data being processed in
addition to the location of the JASPAR database, this being a source of
required data. For example:

.. code:: shell

  tools/data/prepare_data . ../jaspar2020/JASPAR2020.sqlite

Finally, the synonym data is produced as follows:

.. code:: shell

  tools/data/export_data

This tool processes the data inputs and produces the final data in the current
directory (or in any indicated location). It uses an approach employing the
SQLite database system.


Updating Databases
------------------

Deployment scripts are provided with UniBind and JASPAR for updating their
databases with generated synonyms data. These scripts invoke programs supplied
in this distribution: ``update_unibind`` and ``update_jaspar`` respectively.


Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
