-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

begin;

-- Remove a primary key constraint from the table.

alter table synonyms drop constraint synonyms_pkey;

-- Remove a text search index from the synonym field.

drop index synonyms_index_simple;

-- Remove the surrogate key constraint.

alter table synonyms alter column id drop identity;

-- Update the statistics.

analyze synonyms;

commit;
