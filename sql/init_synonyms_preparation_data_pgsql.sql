-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Gene synonyms prepared directly from gene_info.

alter table gene_synonyms add primary key(gene_id, gene_synonym);
analyze gene_synonyms;

-- Profile records from JASPAR.

alter table jaspar_profiles add primary key(jaspar_id, jaspar_version);
analyze jaspar_profiles;

-- Protein records from JASPAR.

alter table jaspar_proteins add primary key(jaspar_id, jaspar_version, uniprot_ac);
analyze jaspar_proteins;

-- UniProt identifier-to-accession mapping.

alter table uniprot_accessions add primary key(uniprot_id, uniprot_ac);
analyze uniprot_accessions;

-- UniProt identifier-to-gene mapping.

alter table uniprot_genes add primary key(uniprot_id, gene_id);
analyze uniprot_genes;

-- Computed UniProt accession-to-gene mapping.

insert into uniprot_mapping
    select distinct uniprot_ac, gene_id
    from uniprot_accessions
    natural join uniprot_genes;

alter table uniprot_mapping add primary key(uniprot_ac, gene_id);
analyze uniprot_mapping;

-- Computed JASPAR profile-to-gene mapping.

insert into jaspar_mapping
    select distinct jaspar_id, jaspar_version, gene_id
    from jaspar_proteins
    natural join uniprot_mapping;

alter table jaspar_mapping add primary key(jaspar_id, jaspar_version, gene_id);
analyze jaspar_mapping;

-- Computed JASPAR profile-to-gene mapping (involving surrogate keys).

insert into profile_genes
    select id as profile_id, gene_id
    from jaspar_profiles
    natural join jaspar_mapping;

alter table profile_genes add primary key(profile_id, gene_id);
analyze profile_genes;

-- Computed concise gene information for profile genes only.

insert into profile_gene_info
    select distinct gene_id, tax_id, gene_symbol
    from gene_synonyms
    where gene_id in (
        select distinct gene_id from profile_genes);

alter table profile_gene_info add primary key(gene_id);
analyze profile_gene_info;

-- Computed concise gene-to-synonym mapping for profile genes only.

insert into profile_gene_synonyms
    select gene_id, gene_synonym
    from gene_synonyms
    where gene_id in (
        select distinct gene_id from profile_genes);

alter table profile_gene_synonyms add primary key(gene_id, gene_synonym);
analyze profile_gene_synonyms;
