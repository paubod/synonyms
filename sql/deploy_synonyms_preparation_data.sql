-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Computed concise gene information for profile genes only.

insert into search_genes
    select gene_id, tax_id, gene_symbol
    from profile_gene_info;

analyze search_genes;

-- Computed concise gene-to-synonym mapping for profile genes only.

insert into search_gene_synonyms
    select nextval('search_gene_synonyms_id_seq'), gene_id, gene_synonym
    from profile_gene_synonyms;

analyze search_gene_synonyms;

-- JASPAR profiles.

insert into search_profiles
    select id, jaspar_id, jaspar_version
    from jaspar_profiles;

analyze search_profiles;

-- Computed JASPAR profile-to-gene mapping (involving surrogate keys).

insert into search_profile_genes
    select nextval('search_profile_genes_id_seq'), profile_id, gene_id
    from profile_genes;

analyze search_profile_genes;
