-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Add a primary key constraint to the table.

alter table synonyms add constraint synonyms_pkey primary key (taxid, geneid, "synonym", symbol);

-- Add a text search index on the synonym field.

create index synonyms_index_simple on synonyms using gin(to_tsvector('simple', synonym));

-- Update the statistics.

analyze synonyms;
