-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Gene synonyms prepared directly from gene_info.

create index gene_synonyms_index on gene_synonyms(gene_id, gene_synonym);
analyze gene_synonyms;

-- Profile records from JASPAR.

create index jaspar_profiles_index on jaspar_profiles(jaspar_id, jaspar_version);
analyze jaspar_profiles;

-- Protein records from JASPAR.

create index jaspar_proteins_index on jaspar_proteins(jaspar_id, jaspar_version, uniprot_ac);
analyze jaspar_proteins;

-- UniProt identifier-to-accession mapping.

create index uniprot_accessions_index on uniprot_accessions(uniprot_id, uniprot_ac);
analyze uniprot_accessions;

-- UniProt identifier-to-gene mapping.

create index uniprot_genes_index on uniprot_genes(uniprot_id, gene_id);
analyze uniprot_genes;

-- Computed UniProt accession-to-gene mapping.
-- Note: SQLite produces duplicate records with a natural join.

insert into uniprot_mapping
    select distinct uniprot_ac, gene_id
    from uniprot_accessions
    inner join uniprot_genes
        on uniprot_accessions.uniprot_id = uniprot_genes.uniprot_id;

create index uniprot_mapping_index on uniprot_mapping(uniprot_ac, gene_id);
analyze uniprot_mapping;

-- Computed JASPAR profile-to-gene mapping.

insert into jaspar_mapping
    select distinct jaspar_id, jaspar_version, gene_id
    from jaspar_proteins
    natural join uniprot_mapping;

create index jaspar_mapping_index on jaspar_mapping(jaspar_id, jaspar_version, gene_id);
analyze jaspar_mapping;

-- Computed JASPAR profile-to-gene mapping (involving surrogate keys).

insert into profile_genes
    select id as profile_id, gene_id
    from jaspar_profiles
    natural join jaspar_mapping;

create index profile_genes_index on profile_genes(profile_id, gene_id);
analyze profile_genes;

-- Computed concise gene information for profile genes only.

insert into profile_gene_info
    select distinct gene_id, tax_id, gene_symbol
    from gene_synonyms
    where gene_id in (
        select distinct gene_id from profile_genes);

create index profile_gene_info_index on profile_gene_info(gene_id);
analyze profile_gene_info;

-- Computed concise gene-to-synonym mapping for profile genes only.

insert into profile_gene_synonyms
    select gene_id, gene_synonym
    from gene_synonyms
    where gene_id in (
        select distinct gene_id from profile_genes);

create index profile_gene_synonyms_index on profile_gene_synonyms(gene_id, gene_synonym);
analyze profile_gene_synonyms;
