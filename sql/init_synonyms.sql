-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

create table synonyms
(
    taxid integer not null,
    geneid integer not null,
    symbol varchar not null,
    "synonym" varchar not null
);
