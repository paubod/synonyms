Workflow
========

This document describes the retrieval of gene synonyms from Entrez Gene's
``gene_info`` file and their incorporation into the JASPAR and UniBind
databases. The ``gene_info`` file can be downloaded from the following
location:

ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz

Details of the file format and other investigations are provided in a separate
`format`_ document and in a document describing considerations related to
development with the `Django`_ Web framework.

.. _`Django`: Django.rst
.. _`format`: Format.rst


.. contents::


Overview
--------

An overview of the workflow is shown below:

.. image:: Workflow.svg

To summarise, the following steps are taken to process the data:

#. Combine UniProt mappings to produce an accession-to-gene mapping.

#. Combine this accession-to-gene mapping with JASPAR profile-to-protein
   mapping, thus obtaining a profile-to-gene mapping (initially involving
   symbolic profile identifiers, ultimately involving numeric profile
   identifiers).

#. Combine gene synonym details to obtain a profile-to-synonym mapping.


Unpacking the Synonym Collections
---------------------------------

To access individual synonyms in the data, a script is provided to unpack them
into separate records. This script can be run as follows:

.. code:: shell

  gunzip -c gene_info.gz | tools/make_synonyms > synonyms.table

The ``make_synonyms`` tool produces tabular output with one synonym or name
per record, replicating the core fields of each original record for each
single synonym record.

======= ===================================================================
Number  Description
======= ===================================================================
1       Taxonomy identifier
2       Gene identifier
3       Symbol
4       Synonym
======= ===================================================================


Selecting Identifiers for UniBind and JASPAR
--------------------------------------------

The name information used by UniBind and JASPAR is not sufficient to
unambiguously identify genes. Consequently, protein information has to be
extracted from JASPAR:

.. code:: shell

  tools/jaspar_proteins ../jaspar/JASPAR2020.sqlite > proteins.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       JASPAR base identifier
2       JASPAR version
3       UniProt accessions
======= ===================================================================

The UniProt accessions need to be found in the UniProt database. To make this
possible, SwissProt/UniProt is downloaded:

.. code:: shell

  wget ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz

Accession details are extracted as follows:

.. code:: shell

  gunzip -c uniprot_sprot.dat.gz | tools/parse_uniprot_accessions > uniprot_accessions

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       UniProt identifier
2       UniProt accession
======= ===================================================================

Gene identifiers are also extracted:

.. code:: shell

  gunzip -c uniprot_sprot.dat.gz | tools/parse_uniprot_genes > uniprot_genes

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       UniProt identifier
2       Gene identifier
======= ===================================================================

Profile information from JASPAR is also required and is extracted as follows:

.. code:: shell

  tools/jaspar_profiles ../jaspar/JASPAR2020.sqlite > profiles.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Record number
2       JASPAR base identifier
3       JASPAR version
======= ===================================================================

Database Approach
+++++++++++++++++

Given an appropriate database name (indicated as ``DATABASE`` below), a
database-driven approach can be used to generate synonym-related data.

For PostgreSQL, the following command is used:

.. code:: shell

  tools/pgsql/prepare_synonyms DATABASE synonyms.table profiles.jaspar proteins.jaspar uniprot_accessions uniprot_genes

For SQLite, the following is used instead:

.. code:: shell

  tools/sqlite/prepare_synonyms DATABASE synonyms.table profiles.jaspar proteins.jaspar uniprot_accessions uniprot_genes

This should produce the following files:

- ``genes.jaspar``
- ``synonyms.jaspar``
- ``profile_genes.jaspar``

Shell Script Approach
+++++++++++++++++++++

A shell-script-driven approach involves slightly more effort and is described
`separately`_.

.. _`separately`: Shell.rst

Model Summary
+++++++++++++

The above approaches can be used for populating a separate database. In JASPAR
and UniBind themselves, the numbering of profile instances and the population
of a suitable table can be done using an appropriate query or by writing code
to use the Django object-relational mapper.

The correspondence between generated files and Django models is as follows:

================= =========================================================
Model             File
================= =========================================================
``Gene``          ``genes.jaspar``
``Synonym``       ``synonyms.jaspar``
``Profile``       ``profiles.jaspar``
``ProfileGene``   ``profile_genes.jaspar``
================= =========================================================
