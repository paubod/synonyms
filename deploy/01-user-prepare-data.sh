#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TOOLSDIR="$BASEDIR/tools"

# Define a common storage location.

DATADIR="/var/tmp/synonyms-data"

if [ ! -e "$DATADIR" ] ; then
    mkdir "$DATADIR"
fi

# Should probably work back from the output files, but here we just test to see
# if the generated data is present and skip all the work if it is.

if [ -e "$DATADIR/genes.jaspar" ] && [ -e "$DATADIR/profile_genes.jaspar" ] &&
   [ -e "$DATADIR/synonyms.jaspar" ] ; then

    exit 0
fi

# Download data, storing it in a common place.
# If this repository is obtained and deployed again separately, the data should
# persist and not be retained locally within the repository itself.

"$TOOLSDIR/data/download_data" "$DATADIR"

# Prepare and export the data products.

JASPARDB=`"$TOOLSDIR/data/find_jaspar_data"`

"$TOOLSDIR/data/prepare_data" "$DATADIR" "$JASPARDB"
"$TOOLSDIR/data/export_data" "$DATADIR"
