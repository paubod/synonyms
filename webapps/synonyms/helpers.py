#!/usr/bin/env python

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# See: https://docs.djangoproject.com/en/3.0/howto/custom-lookups/

from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db.models import Lookup

class ConfigSearch(Lookup):

    """
    A full-text search matching operation incorporating the configuration from
    the query. Example:

    Table.objects.filter(column__configsearch=SearchQuery(..., config="simple"))
    """

    lookup_name = "configsearch"

    def as_sql(self, compiler, connection):

        "Return a (query string, parameter sequence) tuple."

        # Incorporate the configuration from any query operand if no
        # configuration is already specified.

        if isinstance(self.rhs, SearchQuery):
            self.lhs = self._transfer(self.rhs, self.lhs)
        elif isinstance(self.lhs, SearchQuery):
            self.rhs = self._transfer(self.lhs, self.rhs)

        # Prepare the query string and parameters, introducing the matching
        # operator.

        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s @@ %s' % (lhs, rhs), params

    def _transfer(self, source, target):

        """
        Transfer the configuration from 'source' to 'target', if 'target' is not
        already configured. Return the applicable target.
        """

        if not isinstance(target, SearchVector) or not target.config:
            return SearchVector(target, config=source.config)
        else:
            return target
