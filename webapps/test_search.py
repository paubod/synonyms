#!/usr/bin/env python3.7

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from search.models import Gene, Profile, Synonym

profiles_with = Profile.objects.filter
genes_with = Gene.objects.filter
synonyms_with = Synonym.objects.filter

genes = genes_with(gene_id__in=synonyms_with(synonym__icontains='leafy').values_list("gene_id"))

print("Gene query:")
print(genes.query)

print("Genes:")
for result in genes:
    print(result.gene_id, result.tax_id, result.symbol)

profiles = profiles_with(genes__in=genes)

print("Profile query:")
print(profiles.query)

print("Profiles:")
for result in profiles:
    print(result.jaspar_id, result.jaspar_version)
