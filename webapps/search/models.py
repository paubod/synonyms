# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import models

class Gene(models.Model):
    gene_id = models.IntegerField(primary_key=True)
    tax_id = models.IntegerField()
    symbol = models.TextField()

    class Meta:
        db_table = 'search_genes'

class Synonym(models.Model):
    id = models.AutoField(primary_key=True)
    gene_id = models.ForeignKey(Gene, db_column='gene_id', on_delete=models.CASCADE)
    synonym = models.TextField()

    class Meta:
        db_table = 'search_gene_synonyms'

class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    jaspar_id = models.CharField(max_length=16)
    jaspar_version = models.CharField(max_length=1)
    genes = models.ManyToManyField(Gene, related_name='profiles', through='ProfileGene')

    class Meta:
        db_table = 'search_profiles'

class ProfileGene(models.Model):
    profile = models.ForeignKey(Profile, db_column='profile_id', on_delete=models.CASCADE)
    gene_id = models.ForeignKey(Gene, db_column='gene_id', on_delete=models.CASCADE)

    class Meta:
        db_table = 'search_profile_genes'
