# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import models

class Synonyms(models.Model):
    id = models.IntegerField(primary_key=True)
    taxid = models.IntegerField()
    geneid = models.IntegerField()
    symbol = models.TextField()
    synonym = models.TextField()

    class Meta:
        managed = False
        db_table = 'synonyms'
