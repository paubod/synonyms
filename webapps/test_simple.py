#!/usr/bin/env python3.7

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.postgres.search import SearchQuery
from django.db.models.fields import Field
from simple.models import Synonyms
from synonyms.helpers import ConfigSearch

Field.register_lookup(ConfigSearch)

print("With supplied search lookup:")
print(Synonyms.objects.filter(synonym__search=SearchQuery('LFY', config="simple")).query)

print("With configsearch lookup:")
print(Synonyms.objects.filter(synonym__configsearch=SearchQuery('LFY', config="simple")).query)

# Without a suitable patch applied to Django, and with the default single-column
# index on the synonym column, the query below will be very slow.

print("Results:")
for result in Synonyms.objects.filter(synonym__configsearch=SearchQuery('LFY', config="simple")):
    print(result.taxid, result.geneid, result.symbol, result.synonym)
